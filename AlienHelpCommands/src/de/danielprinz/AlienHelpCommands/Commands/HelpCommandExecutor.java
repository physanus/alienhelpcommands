package de.danielprinz.AlienHelpCommands.Commands;

import de.danielprinz.AlienHelpCommands.Main;
import de.danielprinz.AlienHelpCommands.Methods.CommandInitializer;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniela on 10.10.2016.
 */
public class HelpCommandExecutor extends CommandInitializer<Main> {

    public HelpCommandExecutor(Main plugin, String command, String description, String... aliases) {
        super(plugin, command, description, aliases);
    }

    @Override
    public boolean execute(CommandSender cs, String label, String[] args) {

        for(String s : this.plugin.commands.get(this.command).getMessage().split("\n")) {
            cs.sendMessage(s);
        }
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender cs, String label, String[] args) {
        List<String> list = new ArrayList<String>();
        return list;
    }

}