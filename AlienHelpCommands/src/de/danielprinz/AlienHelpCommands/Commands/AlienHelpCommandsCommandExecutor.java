package de.danielprinz.AlienHelpCommands.Commands;

import de.danielprinz.AlienHelpCommands.Main;
import de.danielprinz.AlienHelpCommands.Manager.Command;
import de.danielprinz.AlienHelpCommands.Methods.CommandInitializer;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by daniela on 10.10.2016.
 */
public class AlienHelpCommandsCommandExecutor extends CommandInitializer<Main> {

    public AlienHelpCommandsCommandExecutor(Main plugin, String command, String description, String... aliases) {
        super(plugin, command, description, aliases);
    }

    @Override
    public boolean execute(CommandSender cs, String label, String[] args) {

        if(!cs.hasPermission("alienhelpcommands.reload")) {
            cs.sendMessage(this.plugin.prefix + ChatColor.DARK_RED + "You have no permission to execute this command!");
            return false;
        }

        if(args.length == 1) {
            if (args[0].equals("reload")) {
                this.plugin.restartPlugin();
                cs.sendMessage(this.plugin.prefix + ChatColor.GREEN + "The plugin reloaded successfully.");
                return false;
            }
            cs.sendMessage(this.getHelp());
            return false;
        }

        cs.sendMessage(this.getHelp());
        return false;
    }

    private String getHelp() {

        String s = "";

        s += ChatColor.DARK_AQUA + "\n===== Help for AlienHelpCommands =====\n" +
                ChatColor.GOLD + "/ahc reload" + ChatColor.GRAY + " | " + ChatColor.DARK_AQUA + "Reloads the plugin.";

        for(Map.Entry<String, Command> entry : this.plugin.commands.entrySet()) {
            Command cmd = entry.getValue();
            s += ChatColor.GOLD + "\n/" + cmd.getName() + ChatColor.GRAY + " | " + ChatColor.DARK_AQUA + cmd.getHelpMessage();
        }

        return s;

    }

    @Override
    public List<String> tabComplete(CommandSender cs, String label, String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("reload");
        return list;
    }

}
