package de.danielprinz.AlienHelpCommands.Manager;

/**
 * Created by daniela on 10.10.2016.
 */
public class Command {

    String name;
    String helpMessage;
    String[] aliases;
    String message;

    public Command(String name, String message, String helpMessage, String... aliases) {
        this.name = name;
        this.message = message;
        this.helpMessage = helpMessage;
        this.aliases = aliases;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHelpMessage() {
        return helpMessage;
    }

    public void setHelpMessage(String helpMessage) {
        this.helpMessage = helpMessage;
    }

    public String[] getAliases() {
        return aliases;
    }

    public void setAliases(String[] aliases) {
        this.aliases = aliases;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
