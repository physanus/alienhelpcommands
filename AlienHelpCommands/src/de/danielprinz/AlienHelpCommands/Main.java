package de.danielprinz.AlienHelpCommands;

import de.danielprinz.AlienHelpCommands.Commands.AlienHelpCommandsCommandExecutor;
import de.danielprinz.AlienHelpCommands.Commands.HelpCommandExecutor;
import de.danielprinz.AlienHelpCommands.Manager.Command;
import de.danielprinz.AlienHelpCommands.Methods.CommandUnregisterer;
import org.apache.commons.io.FileUtils;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniela on 10.10.2016.
 */
public class Main extends JavaPlugin {

    public FileConfiguration cfg;
    public Map<String, Command> commands = new HashMap<String, Command>();
    public String prefix;


    @Override
    public void onEnable() {
        this.loadPlugin();
    }

    @Override
    public void onDisable() {
        this.unloadPlugin();
    }

    public void restartPlugin() {
        this.unloadPlugin();
        this.reloadConfig();
        this.loadPlugin();
    }

    public void loadPlugin() {

        this.cfg = getConfig();
        this.commands = new HashMap<String, Command>();
        this.prefix = ChatColor.GRAY + "[" + ChatColor.DARK_RED + "AHC" + ChatColor.GRAY + "] " + ChatColor.DARK_AQUA;

        // Register /alienhelpcommands
        new AlienHelpCommandsCommandExecutor(this, "alienhelpcommands", "Shows custom messages upon command", "ahc");

        if(!this.cfg.contains("enabled-commands")) {
            this.error("tooFewCommandsSpecified");
            return;
        }

        ArrayList<String> commands = new ArrayList<String>(this.cfg.getConfigurationSection("enabled-commands").getKeys(true));

        if(commands.size() == 2) {
            if(commands.get(0).equals("command1") &&
               commands.get(1).equals("command2")) {

                this.error("tooFewCommandsSpecified");
                return;
            }
        }

        for(String cmdname : commands) {
            try {

                String cmdnameSmall = cmdname.toLowerCase();

                File file = new File(getDataFolder() + "/" + cmdnameSmall + ".txt");

                if(!file.exists()) {
                    this.error("fileDoesNotExist", cmdnameSmall);
                    return;
                }

                String message = FileUtils.readFileToString(file);
                message = ChatColor.translateAlternateColorCodes('&', message);
                message = message.replaceAll("\r", "");

                ArrayList<String> section = new ArrayList<String>((ArrayList<String>) this.cfg.getList("enabled-commands." + cmdname));

                String helpMessage = section.get(0);
                section.remove(0);

                for(int i = 0; i < section.size(); i++) {
                    section.set(i, section.get(i).toLowerCase());
                }
                String[] alias = new String[section.size()];
                section.toArray(alias);

                Command command = new Command(cmdnameSmall, message, helpMessage, alias);
                this.commands.put(command.getName(), command);

                new HelpCommandExecutor(this, command.getName(), command.getHelpMessage(), command.getAliases());

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void unloadPlugin() {

        if(!this.commands.isEmpty()) {
            for(Map.Entry<String, Command> command : this.commands.entrySet()) {
                CommandUnregisterer.unRegisterBukkitCommand(command.getValue());
            }
        }

    }

    private void error(String error, String... args) {
        if(error.equals("tooFewCommandsSpecified")) {
            //this.cfg.set("enabled-commands", new ArrayList<String>(){{ add("command1"); add("command2"); add("command3"); }});
            this.cfg.set("enabled-commands", new HashMap<String, ArrayList<String>>(){{
                put("command1", new ArrayList<String>() {{ add("Custom help message"); add("alias1"); add("alias2"); }});
                put("command2", new ArrayList<String>() {{ add("Custom help message"); add("alias3"); add("alias4"); }});
            }});
            this.saveConfig();

            this.getLogger().severe("==========================================================================");
            this.getLogger().severe("== You need to specify commands in plugins/AlienHelpCommands/config.yml ==");
            this.getLogger().severe("===== And add the messages to plugins/AlienHelpCommands/command.txt ======");
            this.getLogger().severe("==========================================================================");
            return;
        }

        if(error.equals("fileDoesNotExist")) {
            String s = "";
            for(int i = 0; i < args[0].length(); i++) {s+="=";}
            this.getLogger().severe("============================================================" + s);
            this.getLogger().severe("== The file plugins/AlienHelpCommands/" + args[0] + ".txt does not exist ==");
            this.getLogger().severe("============================================================" + s);
            return;
        }
    }

}
